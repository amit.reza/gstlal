image: docker:latest

variables:
  DOCKER_DRIVER: overlay
  DOCKER_BRANCH: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
  DOCKER_LATEST: $CI_REGISTRY_IMAGE:latest
  BRANCH: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
  COMMIT: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
  NIGHTLY: $CI_REGISTRY_IMAGE:nightly
  TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG

  # don't need git history
  GIT_DEPTH: 1

  TMPDIR: /tmp
  GSTLAL_FIR_WHITEN: 0

before_script:
  - if [ -d ${CI_PROJECT_DIR}/rpmbuild/RPMS ]; then yum -y install ${CI_PROJECT_DIR}/rpmbuild/RPMS/*/*.rpm; fi

cache:
  key: $CI_JOB_NAME
  paths:
    - ccache

stages:
  - deps
  - level0
  - level1
  - level2
  - docker
  - docker-latest
  - test-gstlal
  - test-gstlal-full-build
  - test-gstlal-ugly
  - test-burst
  - test-inspiral
  - test-offline
  - docs
  - nightly-pages

# conda containers
.dependencies: &dependencies
  stage: deps
  variables: &conda-deps-vars
    GIT_STRATEGY: fetch
    IMAGE_TAG: $CI_REGISTRY_IMAGE/$CI_JOB_NAME:$CI_COMMIT_REF_NAME
  before_script: [ ]
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - >
      docker build --pull
      --build-arg CONDA_ENV=$CONDA_ENV
      -t $IMAGE_TAG
      --file .gitlab-ci.Dockerfile.conda-deps
      .
    - docker push $IMAGE_TAG
  needs: [ ]
  only:
    - schedules
    - pushes

dependencies/conda-dev:
  <<: *dependencies
  variables:
    <<: *conda-deps-vars
    CONDA_ENV: dev

dependencies/conda-prod:
  <<: *dependencies
  variables:
    <<: *conda-deps-vars
    CONDA_ENV: prod

#
# build rpms
#

.levelN:rpm: &levelN-rpm-package
  interruptible: true
  image: containers.ligo.org/gstlal/gstlal-dev:lalsuite-master-x86_64
  variables:
    GIT_STRATEGY: fetch
    RPM_BUILD_CPUS: 4
  script:
    - if [ -d rpmbuild ]; then yum -y install rpmbuild/RPMS/x86_64/*.rpm; fi
    - cd ${CI_JOB_NAME#level?:rpm:}
    - ./00init.sh
    - ./configure --enable-gtk-doc $EXTRA_CONFIG_FLAGS
    - make
    - make dist
    # Install dependencies 
    - yum-builddep -y ${CI_JOB_NAME#level?:rpm:}.spec
    - rpmbuild -tb -D "_topdir ${CI_PROJECT_DIR}/rpmbuild" -D 'debug_package %{nil}' -D 'build_cflags ${CFLAGS}' -D 'build_ldflags ${LDFLAGS}' ${CI_JOB_NAME#level?:rpm:}-*.tar.gz
  artifacts:
    expire_in: 18h
    paths:
      - ${CI_PROJECT_DIR}/rpmbuild/RPMS/
  only:
    - schedules
    - tags
    - pushes
    - web

level0:rpm:gstlal:
  <<: *levelN-rpm-package
  stage: level0
  needs: [ ]

level1:rpm:gstlal-ugly:
  <<: *levelN-rpm-package
  stage: level1
  needs:
    - level0:rpm:gstlal

level2:rpm:gstlal-inspiral:
  <<: *levelN-rpm-package
  stage: level2
  needs:
    - level0:rpm:gstlal
    - level1:rpm:gstlal-ugly
  variables:
    EXTRA_CONFIG_FLAGS: "--disable-massmodel"

level2:rpm:gstlal-burst:
  <<: *levelN-rpm-package
  stage: level2
  needs:
    - level0:rpm:gstlal
    - level1:rpm:gstlal-ugly

# Docker Images

.docker:rpm: &docker-rpm
  interruptible: true
  stage: docker
  before_script: [ ]
  script:
    # add RPMs to directory to pass to docker
    - mkdir rpms

    # Copy rpms to new container.
    - mv rpmbuild/RPMS/x86_64/*.rpm rpms

    # Clear out the old rpmbuild directory
    - rm -rf rpmbuild*

    # Build the container:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build --pull -t $DOCKER_BRANCH --file .gitlab-ci.Dockerfile.rl8 .
    - docker push $DOCKER_BRANCH
  only:
    - schedules
    - pushes

docker:rl8:
  <<: *docker-rpm
  needs:
    - level0:rpm:gstlal
    - level1:rpm:gstlal-ugly
    - level2:rpm:gstlal-inspiral
    - level2:rpm:gstlal-burst
  except:
    - /gstlal-([a-z]+-|)[0-9]+\.[0-9]+\.[0-9]+-v[0-9]+/

docker:release:gstlal:rl8:
  <<: *docker-rpm
  needs:
    - level0:rpm:gstlal
  only:
    - /gstlal-[0-9]+\.[0-9]+\.[0-9]+-v[0-9]+/

docker:release:gstlal-ugly:rl8:
  <<: *docker-rpm
  needs:
    - level0:rpm:gstlal
    - level1:rpm:gstlal-ugly
  only:
    - /gstlal-ugly-[0-9]+\.[0-9]+\.[0-9]+-v[0-9]+/

docker:release:gstlal-inspiral:rl8:
  <<: *docker-rpm
  needs:
    - level0:rpm:gstlal
    - level1:rpm:gstlal-ugly
    - level2:rpm:gstlal-inspiral
  only:
    - /gstlal-inspiral-[0-9]+\.[0-9]+\.[0-9]+-v[0-9]+/

docker:release:gstlal-burst:rl8:
  <<: *docker-rpm
  needs:
    - level0:rpm:gstlal
    - level1:rpm:gstlal-ugly
    - level2:rpm:gstlal-burst
  only:
    - /gstlal-burst-[0-9]+\.[0-9]+\.[0-9]+-v[0-9]+/

.docker:conda: &docker-conda
  interruptible: true
  stage: docker
  variables: &docker-conda-vars
    GIT_STRATEGY: fetch
    NUM_CORES: 4
  before_script: [ ]
  script:
    - IMAGE_TAG=$CI_REGISTRY_IMAGE/conda-$CONDA_ENV:$CI_COMMIT_REF_NAME
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - >
      docker build --pull
      --build-arg CI_REGISTRY_IMAGE=$CI_REGISTRY_IMAGE
      --build-arg CI_COMMIT_REF_NAME=$CI_COMMIT_REF_NAME
      --build-arg CONDA_ENV=$CONDA_ENV
      -t $IMAGE_TAG
      --file .gitlab-ci.Dockerfile.conda
      .
    - docker push $IMAGE_TAG
  needs:
    - dependencies:conda:prod
  only:
    - schedules
    - pushes

docker:conda:dev:
  <<: *docker-conda
  variables:
    <<: *docker-conda-vars
    CONDA_ENV: dev
  needs:
    - dependencies/conda-dev

docker:conda:prod:
  <<: *docker-conda
  variables:
    <<: *docker-conda-vars
    CONDA_ENV: prod
  needs:
    - dependencies/conda-prod

latest_image:
  interruptible: true
  stage: docker-latest
  before_script: [ ]
  needs:
    - docker:release:gstlal-inspiral:rl8
  only:
    - /gstlal-inspiral-[0-9]+\.[0-9]+\.[0-9]+-v[0-9]+/
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker tag $DOCKER_BRANCH $DOCKER_LATEST
    - docker push $DOCKER_LATEST
  retry:
    max: 2

# test stages (rl8)
test:gstlal:rl8:
  interruptible: true
  image: containers.ligo.org/gstlal/gstlal-dev:lalsuite-master-x86_64
  stage: test-gstlal
  needs:
    - level0:rpm:gstlal
  script:
    - export GSTLAL_FIR_WHITEN=0
    - gst-inspect-1.0

    # Get the necessary ROM data:
    - git clone https://git.ligo.org/alexander.pace/gstlal-testing-data.git ${GSTLAL_DIR}/gstlal-testing-data
    - export LAL_DATA_PATH=${GSTLAL_DIR}/gstlal-testing-data/

    # Run doctests
    - cd gstlal
    - python3 -m pytest -c pytest.ini -m "not requires_full_build"
  only:
    - schedules
    - pushes
  allow_failure: true

test:gstlal-full-build:rl8:
  interruptible: true
  image: containers.ligo.org/gstlal/gstlal-dev:lalsuite-master-x86_64
  stage: test-gstlal-full-build
  needs:
    - level0:rpm:gstlal
    - level1:rpm:gstlal-ugly
    - level2:rpm:gstlal-burst
    - level2:rpm:gstlal-inspiral
  script:
    - export GSTLAL_FIR_WHITEN=0
    - gst-inspect-1.0

    # Run doctests
    - cd gstlal
    - python3 -m pytest -c pytest.ini -m "requires_full_build"
  only:
    - schedules
    - pushes
  allow_failure: true

test:gstlal-inspiral:rl8:
  interruptible: true
  image: containers.ligo.org/gstlal/gstlal-dev:lalsuite-master-x86_64
  stage: test-inspiral
  needs:
    - level0:rpm:gstlal
    - level1:rpm:gstlal-ugly
    - level2:rpm:gstlal-inspiral
  script:
    - export GSTLAL_FIR_WHITEN=0
    - gst-inspect-1.0

    # Run doctests
    - cd gstlal-inspiral
    - python3 -m pytest -c pytest.ini
  only:
    - schedules
    - pushes
  allow_failure: true

test:gstlal-ugly:rl8:
  interruptible: true
  image: containers.ligo.org/gstlal/gstlal-dev:lalsuite-master-x86_64
  stage: test-gstlal-ugly
  needs:
    - level0:rpm:gstlal
    - level1:rpm:gstlal-ugly
  script:
    # Install RPMs and set up the test environment:
    - export GSTLAL_FIR_WHITEN=0
    - gst-inspect-1.0

    # Run doctests
    - cd gstlal-ugly
    - python3 -m pytest -c pytest.ini
  only:
    - schedules
    - pushes
  allow_failure: true

test:gstlal-burst:rl8:
  interruptible: true
  image: containers.ligo.org/gstlal/gstlal-dev:lalsuite-master-x86_64
  stage: test-burst
  needs:
    - level0:rpm:gstlal
    - level1:rpm:gstlal-ugly
    - level2:rpm:gstlal-burst
    - level2:rpm:gstlal-inspiral
  script:
    - export GSTLAL_FIR_WHITEN=0
    - gst-inspect-1.0
    - cd gstlal-burst
    - python3 -m pytest -c pytest.ini
  only:
    - schedules
    - pushes
  allow_failure: true

test:offline:rl8:
  interruptible: true
  image: containers.ligo.org/gstlal/gstlal-dev:lalsuite-master-x86_64
  stage: test-offline
  needs:
    - level0:rpm:gstlal
    - level1:rpm:gstlal-ugly
    - level2:rpm:gstlal-inspiral
    - level2:rpm:gstlal-burst
    - test:gstlal:rl8
    - test:gstlal-inspiral:rl8
    - test:gstlal-burst:rl8
  script:
   # Set up directory structure and copy over built-dependencies from container:
    - mkdir public
    # Install RPMs and set up the test environment:
    - gst-inspect-1.0

    # Export variables for the offline tutorial
    - export LAL_PATH=/usr
    - export USER=gstlal_CI_test
    - export PYTHONUNBUFFERED=1

    - yum -y install bc

    # Get the necessary ROM data:
    - git clone https://git.ligo.org/alexander.pace/gstlal-testing-data.git ${GSTLAL_DIR}/gstlal-testing-data
    - export LAL_DATA_PATH=${GSTLAL_DIR}/gstlal-testing-data/

    - cd gstlal-inspiral/tests

    # Run the makefile:
    - make -f Makefile.offline_tutorial_test ENABLE_PLOTTING=0

    # Back-up the results docs:
    #- cp -rf ./WEBDIR/gstlal_offline_tutorial ../../public/

  #artifacts:
  #  expire_in: 24h
  #  paths:
  #    - gstlal-inspiral/tests/WEBDIR/gstlal_offline_tutorial
  #    - public
  #  when: always
  only:
    - schedules
    - pushes
  allow_failure: true


# test stages (conda)

test:gstlal:conda:
  interruptible: true
  image: $CI_REGISTRY_IMAGE/conda-dev:$CI_COMMIT_REF_NAME
  stage: test-gstlal
  needs:
    - docker:conda:dev
  before_script: [ ]
  script:
    - cd gstlal
    - python3 -m pytest -c pytest.ini -m "not requires_full_build" --junitxml=report.xml
  only:
    - schedules
    - pushes
  allow_failure: true
  artifacts:
    when: always
    reports:
      junit: gstlal/report.xml

test:gstlal-full-build:conda:
  interruptible: true
  image: $CI_REGISTRY_IMAGE/conda-dev:$CI_COMMIT_REF_NAME
  stage: test-gstlal-full-build
  needs:
    - docker:conda:dev
  before_script: [ ]
  script:
    - cd gstlal
    - python3 -m pytest -c pytest.ini -m "requires_full_build" --junitxml=report-full-build.xml
  only:
    - schedules
    - pushes
  allow_failure: true
  artifacts:
    when: always
    reports:
      junit: gstlal/report-full-build.xml

test:gstlal-inspiral:conda:
  interruptible: true
  image: $CI_REGISTRY_IMAGE/conda-dev:$CI_COMMIT_REF_NAME
  stage: test-inspiral
  needs:
    - docker:conda:dev
  before_script: [ ]
  script:
    - cd gstlal-inspiral
    - python3 -m pytest -c pytest.ini --junitxml=report-inspiral.xml
  only:
    - schedules
    - pushes
  allow_failure: true
  artifacts:
    when: always
    reports:
      junit: gstlal/report-inspiral.xml

test:gstlal-ugly:conda:
  interruptible: true
  image: $CI_REGISTRY_IMAGE/conda-dev:$CI_COMMIT_REF_NAME
  stage: test-gstlal-ugly
  needs:
    - docker:conda:dev
  before_script: [ ]
  script:
    - cd gstlal-ugly
    - python3 -m pytest -c pytest.ini --junitxml=report-ugly.xml
  only:
    - schedules
    - pushes
  allow_failure: true
  artifacts:
    when: always
    reports:
      junit: gstlal/report-ugly.xml

test:gstlal-burst:conda:
  interruptible: true
  image: $CI_REGISTRY_IMAGE/conda-dev:$CI_COMMIT_REF_NAME
  stage: test-burst
  needs:
    - docker:conda:dev
  before_script: [ ]
  script:
    - cd gstlal-burst
    - python3 -m pytest -c pytest.ini --junitxml=report-burst.xml
  only:
    - schedules
    - pushes
  allow_failure: true
  artifacts:
    when: always
    reports:
      junit: gstlal/report-burst.xml

test:offline:conda:
  interruptible: true
  image: $CI_REGISTRY_IMAGE/conda-dev:$CI_COMMIT_REF_NAME
  stage: test-offline
  needs:
    - docker:conda:dev
    - test:gstlal:conda
    - test:gstlal-inspiral:conda
    - test:gstlal-burst:conda
  before_script: [ ]
  script:
    # Set up directory structure and copy over built-dependencies from container:
    - mkdir public

    # Export variables for the offline tutorial
    - export LAL_PATH=$CONDA_PREFIX
    - export USER=gstlal_CI_test

    # Get the necessary ROM data:
    - git clone https://git.ligo.org/alexander.pace/gstlal-testing-data.git ${GSTLAL_DIR}/gstlal-testing-data
    - export LAL_DATA_PATH=${GSTLAL_DIR}/gstlal-testing-data/

    - cd gstlal-inspiral/tests

    # Run tests
    - make -f Makefile.offline_tutorial_test ENABLE_PLOTTING=0

  only:
    - schedules
    - pushes
  allow_failure: true

# Documentation

docs:
  interruptible: true
  image: $CI_REGISTRY_IMAGE/conda-dev:$CI_COMMIT_REF_NAME
  stage: docs
  before_script: [ ]
  script:
    - |
      export DEBIAN_FRONTEND=noninteractive
      export TZ=America/New_York
      apt-get --allow-releaseinfo-change update -y
      apt-get install -y dvipng texlive-latex-base texlive-latex-extra
      source /opt/conda/etc/profile.d/conda.sh
      conda activate gstlal-dev
      mkdir -p docs/
      cd doc; make html IS_CI=1
      cd ..; cp -rf doc/_build/* docs/
  needs:
    - docker:conda:dev
  artifacts:
    paths:
      - docs
  only:
    - pushes
    - schedules

pages:
  interruptible: true
  stage: nightly-pages
  before_script: [ ]
  script:
    # copy doc artifacts
    - mkdir -p public/
    - cp -r docs/* public/
  needs:
    - docs
  artifacts:
    paths:
      - public
  only:
    - master@lscsoft/gstlal
    - schedules
  except:
    - web
    - pushes
