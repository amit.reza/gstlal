.. _psd-generation:

PSD Generation
================

Using this workflow configuration (``config.yml``), we can generate a
median-averaged PSD across 100000 seconds of Hanford and Livingston data in O2:

.. code:: bash

  # general options
  start: 1187000000
  stop: 1187100000
  ifos: H1L1
  
  # data discovery options
  source:
    frame-type:
      H1: H1_GWOSC_O2_16KHZ_R1
      L1: L1_GWOSC_O2_16KHZ_R1
    channel-name:
      H1: GWOSC-16KHZ_R1_STRAIN
      L1: GWOSC-16KHZ_R1_STRAIN
    sample-rate: 4096
    frame-segments-file: segments.xml.gz
    frame-segments-name: datasegments
  
  # psd options
  psd:
    fft-length: 8
    sample-rate: 4096
  
  # condor options
  condor:
    accounting-group: your.accounting.group
    profile: ldas


This sets general options such as the start and end times and the detectors to
analyze (H1 and L1). Data discovery options specify how to retrieve the strain
data as well as specifying the file used for science segments (``segments.xml.gz``),
and the sampling rate. PSD-specific options such as the FFT stride used.

Finally,
workflow-specific options (via HTCondor) which specify options like the accounting group
and the grid profile to use. If you have not done so yet, you can install grid profiles
locally and check which grid profiles are currently available via:

.. code:: bash

  $ gstlal_grid_profile install
  $ gstlal_grid_profile list  # ldas should show up as one of the possible options


To launch the workflow via Condor with the configuration above, run:

.. code:: bash

  $ gstlal_query_gwosc_segments 1187000000 1187100000 H1L1
  $ gstlal_psd_workflow -c config.yml
  $ condor_submit_dag psd_dag.dag
