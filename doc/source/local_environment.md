# Local Development Environment

The local development workflow consists of a few key points:

- Managed conda environments using [conda-flow](https://git.ligo.org/james.kennington/conda-flow)
- Using integrated development environments (IDEs) like [PyCharm]()/[CLion]()
- Running applications that can consume `gstlal`, like [Jupyter Notebook]()

## Creating the environment

Thanks to conda-flow, creating the environment is simple. Before we can use conda-flow, it must be installed. In
whatever environment you prefer, install conda-flow from pip:

```bash
pip install conda-flow
```

Once conda-flow is installed, There are locked environment files contained within the gstlal repo under
`gstlal/gstlal/share/conda/envs`. Using conda-flow, we can make sure the local development conda environment is built:

```bash
conda-flow activate -n gstlal-dev -c /path/to/gstlal/gstlal/share/conda/conda-flow.yml
```

## Activating the environment

The `activate` command within conda-flow is done through subprocesses, and consequently will *not* affect the parent
process, such as the shell from which conda-flow is called. This is done to prevent unintended side effects; however, it
also means that unlike `conda activate`, `conda-flow activate` will not activate the environment inside the shell. If
you wish to activate the environment inside the shell, run `conda activate gstlal-dev`.

## Using Developer Tools

To use an IDE to develop `gstlal`, you will first need to start your IDE from within the appropriate conda environment.
For example, to launch the PyCharm IDE, run:

```bash
conda-flow activate -n gstlal-dev --run-after "open -na ""PyCharm.app""" -c /path/to/gstlal/gstlal/share/conda/conda-flow.yml
```

### Python Development

Note that the python source modules are not in a typical pythonic package structure (due to use of GNU build for c code
within gstlal). This can present problems with package indexing / imports in the IDE. The solution is to provide the IDE
with a map of the proper import paths, which can be done via two ways:

1. Build `gstlal` (or at least the python components) and add the build directory as a source directory in your IDE
1. Create a new source directory full of symlinks to the source files with a structure that mimicks the import paths.
   There is a utility for constructing such symlinks at `gstlal/gstlal/share/conda/dev_links.py`

## Launching Applications

Conda-flow is capable of running arbitrary commands after activating the environment in the subprocess which is useful
for launching applications in a controlled way. For example, to run a jupyter notebook:

```bash
conda-flow activate -n gstlal-dev --run-after "jupyter notebook" -c /path/to/gstlal/gstlal/share/conda/conda-flow.yml
```

