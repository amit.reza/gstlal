.. _installation:

Installation
=============

There are various ways to get started with GstLAL:

  * :ref:`Install the latest release <install-release>`. Pre-built packages are available through various mechanisms.
  * :ref:`Use a version provided in an IGWN reference distribution <install-igwn>`. This option is available to members of the International Gravitational-Wave Obervatory Network (IGWN). 
  * :ref:`Building the package from source <install-source>`. This is needed for users who wish to contribute to the project.

.. _install-release:

Installing the latest release
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Docker container (recommended)
""""""""""""""""""""""""""""""

The following should pull a GstLAL container and enter an environment with
GstLAL and all its dependencies pre-installed:

.. code:: bash

    $ docker run -it --rm containers.ligo.org/lscsoft/gstlal:latest

Note that you will need `Docker <https://docs.docker.com/get-docker/>`_
installed. If that is not an option (Docker requires sudo privileges), you can
instead use `Singularity
<https://sylabs.io/guides/3.7/user-guide/quick_start.html>`_ in place of Docker,
which is available on many shared computing resources such as XSEDE and the OSG:

.. code:: bash

    $ singularity run docker://containers.ligo.org/lscsoft/gstlal:latest

Conda installation
"""""""""""""""""""

Install conda using the `miniconda <https://docs.conda.io/projects/conda/en/latest/user-guide/install/>`_ installer, then run:

.. code:: bash

    $ conda install -c conda-forge gstlal-inspiral

In order to check your installation, you can use:

.. code:: bash

    $ conda list gstlal-inspiral  # to check which version is installed
    $ gstlal_play --help

.. warning::
   These packages don't make use of any :ref:`math optimizations <install-math_optimizations>`
   and is not currently recommended for production or larger-scale analyses.

.. _install-igwn:

IGWN distributions of GstLAL
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you are an IGWN member and have access to shared computing resources,
up-to-date GstLAL libraries are available in the set of reference software
environments maintained by the IGWN Computing and Software Working Group.

LIGO Data Grid (LDG)
"""""""""""""""""""""

GstLAL packages are installed and available by default on the LDG. You can start
using the GstLAL library immediately:

.. code:: bash

    $ gstlal_play --help

IGWN Conda Distribution
""""""""""""""""""""""""

GstLAL is also available on the IGWN Conda Distribution in a variety of
pre-packaged environments. For more information, see
`computing.docs.ligo.org/conda/ <https://computing.docs.ligo.org/conda/>`_.

.. _install-source:

Building from source
^^^^^^^^^^^^^^^^^^^^^

Building from source is required for development (bug fixes, new features,
documentation improvements). You can check out the latest source of GstLAL from
git:

.. code:: bash

    $ git clone https://git.ligo.org/lscsoft/gstlal.git
    $ cd gstlal


Building and installing from source follows the normal GNU build procedures involving:

 1. ./00init.sh 
 2. ./configure 
 3. make 
 4. make install

Since GstLAL is a collection of packages, there is a required build order to
install the packages:

 1. gstlal
 2. gstlal-ugly
 3. gstlal-burst / gstlal-inspiral  (any order)

If you are building from source, you will also need to install all dependencies
before building GstLAL, including:

  * fftw
  * gsl
  * gstreamer
  * lalsuite
  * ldas-tools-framecpp
  * numpy
  * pygobject
  * python-ligo-lw
  * scipy

These dependencies can be installed in various ways, including conda, your
favorite package manager (apt/yum), or from source. We also provide containers
that are suitable for development.

Singularity container
""""""""""""""""""""""

A development container is provided with all necessary dependencies to install
GstLAL from source. Singularity also has extra features that make it possible to
create writable containers, making it easy to get started with development:

.. code:: bash

    $ singularity build --sandbox --fix-perms gstlal-dev docker://containers.ligo.org/lscsoft/gstlal:master

This will pull a container from the container registry from the main branch and
builds it in 'sandbox' mode into ``/gstlal-dev``, which allows one to invoke it
in writable mode once it's built, and is needed to install software into the
container. This may take a few minutes to set up compared to a normal pull.

Once that's finished, you can enter the container in writable mode to install GstLAL from source:

.. code:: bash

    $ singularity run --writable gstlal-dev
    $ git clone https://git.ligo.org/lscsoft/gstlal.git
    $ cd gstlal

.. note::

   It's possible to run into issues when adding bind mounts to a writable
   Singularity container depending on how Singularity is configured. This
   may cause the following error to occur:

   .. code:: bash

       $ singularity run --writable gstlal-dev
       WARNING: By using --writable, Singularity can't create /cvmfs destination automatically without overlay or underlay
       FATAL:   container creation failed: mount /cvmfs->/cvmfs error: while mounting /cvmfs: destination /cvmfs doesn't exist in container

   In this case, one way to resolve this error is to add directories in the
   container for each of the bind mounts explicitly, e.g.,:

   .. code:: bash

       $ mkdir gstlal-dev/cvmfs
       $ singularity run --writable -B /cvmfs gstlal-dev
       Singularity>


Now you can follow the normal GNU build procedures to build and install GstLAL.
It is also recommended to install GstLAL into the container's ``/usr``
directory, done at the configure step, e.g.

.. code:: bash

    $ ./configure --prefix /usr


.. _install-math_optimizations:

Math-optimized Libraries
^^^^^^^^^^^^^^^^^^^^^^^^^^

GstLAL relies heavily on Linear Algebra and FFT routines to drive various
signal-processing components within the library. Being able to accelerate these
math-heavy routines which rely on BLAS with MKL considerably improves performance
and introduces significant speedups. The containers
that are available via Docker and Singularity are linked against MKL to take
advantage of these optimizations. There is no additional work needed from the user.

Work is ongoing to provide Conda packages to allow for similar optimizations.
