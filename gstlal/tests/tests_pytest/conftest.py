"""Pytest configuration

"""

import pytest

import lal
import lalsimulation


@pytest.fixture(scope="session")
def reference_psd():
	"""generate an example reference PSD.
	"""
	sample_rate = 32
	psd = lal.CreateREAL8FrequencySeries(
		name="psd",
		epoch=lal.LIGOTimeGPS(0),
		f0=0.,
		deltaF=(1. / sample_rate),
		sampleUnits=lal.Unit("strain^2 s"),
		length=(32 * 16384),
	)
	lalsimulation.SimNoisePSDaLIGODesignSensitivityP1200087(psd, 0.)

	return psd
