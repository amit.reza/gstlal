"""Unit tests for kernels module

"""

import numpy

from gstlal import kernels


def test_one_second_highpass_kernel():
	"""Test one_second_highpass_kernel.
	"""
	kernel = kernels.one_second_highpass_kernel(rate=4096, cutoff=12)
	assert isinstance(kernel, numpy.ndarray)


def test_fixed_duration_bandpass_kernel():
	"""Test fixed_duration_bandpass_kernel.
	"""
	kernel = kernels.fixed_duration_bandpass_kernel(rate=4096)
	assert isinstance(kernel, numpy.ndarray)
