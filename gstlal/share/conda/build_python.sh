# Script for rebuilding Python *only* (essentially using make install in python dirs to cp files)
# run this script after making changes to python code and before restarting Python session
# in whatever local application you're running (bash, notebook, etc) to pickup dev changes
# Script for building GstLAL from source

# Discover repository paths
SCRIPT_PATH="$(
  cd -- "$(dirname "$0")" >/dev/null 2>&1
  pwd -P
)"
CONDA_PATH=$(
  builtin cd $SCRIPT_PATH
  pwd
)

source $CONDA_PATH/set_env.sh

# Define GNU sequence function
function install_sequence() {
  clear
  echo "Running make install"
  make install
  echo Done
  sleep 3
}

# Move to gstlal location
cd $REPO_PATH/gstlal/python
echo "Installing: gstlal"
install_sequence

# Move to gstlal location
cd $REPO_PATH/gstlal-ugly/python
echo "Installing: gstlal-ugly"
install_sequence

# Move to gstlal location
cd $REPO_PATH/gstlal-burst/python
echo "Installing: gstlal-burst"
install_sequence

# Move to gstlal location
cd $REPO_PATH/gstlal-inspiral/python
echo "Installing: gstlal-inspiral"
install_sequence
