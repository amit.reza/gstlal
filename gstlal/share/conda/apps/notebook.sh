# Launch a jupyter notebook in the dev env

# Discover repository paths (leading underscores to avoid collision with set_env
_SCRIPT_PATH="$(
  cd -- "$(dirname "$0")" >/dev/null 2>&1
  pwd -P
)"
_APPS_PATH=$(
  builtin cd $_SCRIPT_PATH
  pwd
)
_CONDA_PATH=$(
  builtin cd $_APPS_PATH
  cd ..
  pwd
)
_SHARE_PATH=$(
  builtin cd $_CONDA_PATH
  cd ..
  pwd
)

source $_CONDA_PATH/set_env.sh notebook
jupyter notebook --notebook-dir=$_SHARE_PATH/notebooks
