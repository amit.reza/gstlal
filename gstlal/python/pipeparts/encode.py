"""Module for encoding elements

"""

from gstlal.pipeparts import pipetools


## Adds a <a href="@gstpluginsgooddoc/gst-plugins-good-plugins-wavenc.html">wavenc</a> element to a pipeline with useful default properties
def wav(pipeline: pipetools.Pipeline, src: pipetools.Element) -> pipetools.Element:
	"""Format source into the wav format

	Args:
		pipeline:
			Gst.Pipeline, the pipeline to which the new element will be added
		src:
			Gst.Element, the source element


	References:
		[1] wavenc docs: https://gstreamer.freedesktop.org/documentation/wavenc/index.html?gi-language=python

	Returns:
		Element, the src encoded as wav format
	"""
	return pipetools.make_element_with_src(pipeline, src, "wavenc")


## Adds a <a href="@gstpluginsbasedoc/gst-plugins-base-plugins-vorbisenc.html">vorbisenc</a> element to a pipeline with useful default properties
def vorbis(pipeline: pipetools.Pipeline, src: pipetools.Element) -> pipetools.Element:
	"""This element encodes raw float audio into a Vorbis stream

	Args:
		pipeline:
			Gst.Pipeline, the pipeline to which the new element will be added
		src:
			Gst.Element, the source element

	References:
		[1] vorbisenc docs: https://gstreamer.freedesktop.org/documentation/vorbis/vorbisenc.html?gi-language=python

	Returns:
		Element, the source encoded as a Vorbis stream
	"""
	return pipetools.make_element_with_src(pipeline, src, "vorbisenc")


def theora(pipeline: pipetools.Pipeline, src: pipetools.Element, **properties: dict) -> pipetools.Element:
	"""This element encodes raw video into a Theora stream

	Args:
		pipeline:
			Gst.Pipeline, the pipeline to which the new element will be added
		src:
			Gst.Element, the source element
		**properties:
			dict, keyword arguments to be set as element properties

	References:
		[1] theoraenc docs: https://gstreamer.freedesktop.org/documentation/theora/theoraenc.html?gi-language=python

	Returns:
		Element, the source encoded as a Theora stream
	"""
	return pipetools.make_element_with_src(pipeline, src, "theoraenc", **properties)


## Adds a <a href="@gstpluginsgooddoc/gst-plugins-good-plugins-flacenc.html">flacenc</a> element to a pipeline with useful default properties
def flac(pipeline: pipetools.Pipeline, src: pipetools.Element, quality=0, **properties):
	"""Encoded sources as FLAC streams. FLAC is a Free Lossless Audio Codec.
	FLAC audio can directly be written into a file, or embedded into containers such as oggmux or matroskamux.

	Args:
		pipeline:
			Gst.Pipeline, the pipeline to which the new element will be added
		src:
			Gst.Element, the source element
		quality:
			int
		**properties:
			dict, keyword arguments to be set as element properties

	References:
		[1] https://gstreamer.freedesktop.org/documentation/flac/flacenc.html?gi-language=python

	Returns:
		Element, the source encoded as a FLAC stream
	"""
	return pipetools.make_element_with_src(pipeline, src, "flacenc", quality=quality, **properties)


def igwd_parse(pipeline: pipetools.Pipeline, src: pipetools.Element, **properties) -> pipetools.Element:
	"""Parse byte streams into whole IGWD frame files (https://dcc.ligo.org/cgi-bin/DocDB/ShowDocument?docid=329)

	Args:
		pipeline:
			Gst.Pipeline, the pipeline to which the new element will be added
		src:
			Gst.Element, the source element
		**properties:

	References:
		Implementation gstlal/gstlal-ugly/gst/framecpp/framecpp_igwdparse.cc

	Returns:
		Element
	"""
	return pipetools.make_element_with_src(pipeline, src, "framecpp_igwdparse", **properties)


## Adds a <a href="@gstlalgtkdoc/GstTSVEnc.html">lal_nxydump</a> element to a pipeline with useful default properties
def tsv(pipeline: pipetools.Pipeline, src: pipetools.Element, segment: pipetools.Segment = None) -> pipetools.Element:
	"""Converts audio time-series to tab-separated ascii text, a format compatible with most plotting utilities.
	The output is multi-column tab-separated ASCII text.  The first column is the time, the remaining columns are
	the values of the channels in order.

	Args:
		pipeline:
			Gst.Pipeline, the pipeline to which the new element will be added
		src:
			Gst.Element, the source element
		segment:
			Segment, default None, a ligo.segments.segment

	Returns:
		Element
	"""
	if segment is not None:
		return pipetools.make_element_with_src(pipeline, src, "lal_nxydump", start_time=segment[0].ns(), stop_time=segment[1].ns())
	else:
		return pipetools.make_element_with_src(pipeline, src, "lal_nxydump")


## Adds a <a href="@gstpluginsbasedoc/gst-plugins-base-plugins-uridecodebin.html">uridecodebin</a> element to a pipeline with useful default properties
def uri_decode_bin(pipeline: pipetools.Pipeline, uri: str, caps: pipetools.Caps = "application/x-igwd-frame,framed=true", **properties) -> pipetools.Element:
	"""Decodes data from a URI into raw media. It selects a source element that can handle the
	given scheme and connects it to a decodebin.

	Args:
		pipeline:
			Gst.Pipeline, the pipeline to which the new element will be added
		uri:
			str, URI to decode
		caps:
			Gst.Caps, The caps on which to stop decoding. (NULL = default)
		**properties:

	References:
		[1] https://gstreamer.freedesktop.org/documentation/playback/uridecodebin.html?gi-language=python

	Returns:
		Element
	"""
	return pipetools.make_element_with_src(pipeline, None, "uridecodebin", uri=uri, caps=None if caps is None else pipetools.Gst.Caps.from_string(caps), **properties)
