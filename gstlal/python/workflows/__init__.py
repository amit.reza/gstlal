# Copyright (C) 2021  Patrick Godwin (patrick.godwin@ligo.org)
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import os
from typing import Optional

import jinja2

from gstlal.config import Config


def write_makefile(config: Config, template: str, config_filename="config.yml", path: Optional[str] = None, **kwargs) -> None:
	"""Generate a Makefile based on a configuration and template.

	Args:
		config:
			Config, a configuration to use to fill in Makefile template
		template:
			str, name of the Makefile template
		path:
			str, default $PWD, the path to write the Makefile to
		**kwargs:
			any extra key-value pairs used for the template

	"""
	if not path:
		path = os.getcwd()

	template_loader = jinja2.PackageLoader("gstlal.workflows", "templates")
	template_env = jinja2.Environment(loader=template_loader, trim_blocks=True, lstrip_blocks=True)
	template = template_env.get_template(template)

	makefile = template.render(config=config, **kwargs)
	makefile = makefile.replace("config.yml", config_filename)
	with open(os.path.join(path, "Makefile"), "w") as f:
		f.write(makefile)
