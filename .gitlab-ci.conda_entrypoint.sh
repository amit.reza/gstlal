#!/usr/bin/env bash

. /opt/conda/etc/profile.d/conda.sh
conda activate gstlal-${CONDA_ENV}

exec "$@"
