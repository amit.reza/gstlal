/*
 * Copyright (C) 2022 Rachael Huxford, Chad Hanna
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */


/**
 * SECTION:gstlal_integrate
 * @short_description:  Integrate timeseries between two points.
 *
 */


/*
 * ========================================================================
 *
 *                                  Preamble
 *
 * ========================================================================
 */

/*
 * stuff from gsl library
 */
#include <gsl/gsl_vector.h>
#include <gsl/gsl_statistics.h>
#include <gsl/gsl_matrix.h>

/*
 * stuff from the C library
 */
#include <math.h>


/*
 * stuff from glib/gstreamer
 */
#include <glib.h>
#include <gst/gst.h>
#include <gst/audio/audio.h>
#include <gst/base/gstadapter.h>
#include <gst/base/gstbasetransform.h>


/*
 * stuff from LAL
 */
#include <lal/LALDatatypes.h>
#include <lal/LALStdlib.h>
#include <lal/Date.h>
#include <lal/Sequence.h>
#include <lal/TimeSeries.h>
#include <lal/FrequencySeries.h>
#include <lal/TimeFreqFFT.h>
#include <lal/Units.h>
#include <lal/Window.h>
#include <lal/Units.h>
#include <lal/EPSearch.h>


/*
 * our own stuff
 */
#include <gstlal/gstlal.h>
#include <gstlal/gstaudioadapter.h>
#include <gstlal/gstlal_tags.h>
#include <gstlal/gstlal_debug.h>
#include <gstlal_integrate.h>


static const LIGOTimeGPS GPS_ZERO = {0, 0};



/*
 * ============================================================================
 *
 *                           GStreamer Boiler Plate
 *
 * ============================================================================
 */


#define GST_CAT_DEFAULT gstlal_integrate_debug
GST_DEBUG_CATEGORY_STATIC(GST_CAT_DEFAULT);


G_DEFINE_TYPE_WITH_CODE(
	GSTLALIntegrate,
	gstlal_integrate,
	GST_TYPE_BASE_TRANSFORM,
	GST_DEBUG_CATEGORY_INIT(GST_CAT_DEFAULT, "lal_integrate", 0, "lal_integrate element")
);

static GstStaticPadTemplate sink_template =
	GST_STATIC_PAD_TEMPLATE ("sink",
	GST_PAD_SINK,
	GST_PAD_ALWAYS,
	GST_STATIC_CAPS ("audio/x-raw, " \
		"format = (string) {" GST_AUDIO_NE(F32) ", " GST_AUDIO_NE(F64) "}, " \
		"rate =  (int) {1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768}, " \
		"channels = " GST_AUDIO_CHANNELS_RANGE ", " \
		"layout = (string) interleaved, " \
		"channel-mask = (bitmask) 0")
	);


static GstStaticPadTemplate src_template =
	GST_STATIC_PAD_TEMPLATE ("src",
	GST_PAD_SRC,
	GST_PAD_ALWAYS,
	GST_STATIC_CAPS ("audio/x-raw, " \
		"format = (string) {" GST_AUDIO_NE(F32) ", " GST_AUDIO_NE(F64) "}, " \
		"rate =  (int) {1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768}, " \
		"channels = " GST_AUDIO_CHANNELS_RANGE ", " \
		"layout = (string) interleaved, " \
		"channel-mask = (bitmask) 0")

	);



/*
 * ============================================================================
 *
 *                                 Parameters
 *
 * ============================================================================
 */



/*
 * Virtual method protototypes
 */

static gboolean start(GstBaseTransform *trans);
static gboolean stop(GstBaseTransform *trans);
static GstMessage *gstlal_integrate_message_new64(GSTLALIntegrate *element, gdouble int_max, gdouble int_max_time);
static GstMessage *gstlal_integrate_message_new32(GSTLALIntegrate *element, gfloat int_max, gdouble int_max_time);
static void free_queue_element (void *data, void *userData);

/*
 * ============================================================================
 *
 *                                 Utilities
 *
 * ============================================================================
 */

/*
 * integrate()
 */


static GstFlowReturn integrate32(GSTLALIntegrate *element, float *output, guint *size)
{
	gsl_vector_float *buf_vector32 = gsl_vector_float_alloc(*size);
	gfloat max_int_val = -G_MAXFLOAT;
	gdouble max_int_time = 0.0;

	GstFlowReturn result = GST_FLOW_OK;
	/*
 	* Initialize vector for buffer with current buffer's contents
 	*/
	gst_audioadapter_copy_samples(element->adapter, buf_vector32->data, *size, NULL, NULL);
	/*
	 * Calculate the sum for each value in inbuf.
	 */
	gfloat this_int, last_val;
	gsl_vector_float_view output_data = gsl_vector_float_view_array(output, *size);
	guint i;
	for (i=0; i<*size; i++){
		gfloat sum;
		if (i==0){
			sum = gsl_vector_float_get(buf_vector32, i) + element->last_buf_sum;
			last_val = sum;
		}
		else{
			sum = gsl_vector_float_get(buf_vector32, i) + last_val;
			last_val = sum;
		}
		/* From the sum, calculate the integral temp_dur back in history & push sum to queue */
		gfloat *temp_dur_val = g_queue_pop_tail(element->sum_queue);
		this_int = sum - *temp_dur_val;
		*temp_dur_val = sum;
		g_queue_push_head(element->sum_queue, temp_dur_val);
		/* Normalize the integral value */
		this_int = this_int / (element->sample_rate);
		/* Store this int for message if greatest in buffer */
		if (this_int > max_int_val){
			max_int_val = this_int;
			max_int_time = element->timestamp + (i * element->sample_rate);
		}
		/* Put integral value in output buffer via output_data */
		gsl_vector_float_set(&output_data.vector, i, this_int);
	}
	element->last_buf_sum = last_val;
	gsl_vector_float_free(buf_vector32);

	/*
 	* SEND MSG
 	*/
	gst_element_post_message(GST_ELEMENT(element), gstlal_integrate_message_new32(element, max_int_val, max_int_time));

	return result;
}


static GstFlowReturn integrate64(GSTLALIntegrate *element, double *output, guint *size)
{
	gsl_vector *buf_vector64 = gsl_vector_alloc(*size);
	gdouble max_int_val = -G_MAXDOUBLE;
	gdouble max_int_time = 0.0;

	GstFlowReturn result = GST_FLOW_OK;
	/*
 	* Initialize vector for buffer with current buffer's contents
 	*/
	gst_audioadapter_copy_samples(element->adapter, buf_vector64->data, *size, NULL, NULL);
	/*
	 * Calculate the sum for each value in inbuf.
	 */
	gdouble this_int, last_val;
	gsl_vector_view output_data = gsl_vector_view_array(output, *size);
	guint i;
	for (i=0; i<*size; i++){
		gdouble sum;
		if (i==0){
			sum = gsl_vector_get(buf_vector64, i) + element->last_buf_sum;
			last_val = sum;
		}
		else{
			sum = gsl_vector_get(buf_vector64, i) + last_val;
			last_val = sum;
		}
		/* From the sum, calculate the integral temp_dur back in history & push sum to queue */
		gdouble *temp_dur_val = g_queue_pop_tail(element->sum_queue);
		this_int = sum - *temp_dur_val;
		*temp_dur_val = sum;
		g_queue_push_head(element->sum_queue, temp_dur_val);

		/* Normalize the integral value */
		this_int = this_int / (element->sample_rate);
		/* Store this int for message if greatest in buffer */
		if (this_int > max_int_val){
			max_int_val = this_int;
			max_int_time = element->timestamp + (i * element->sample_rate);
		}
		/* Put integral value in output buffer via output_data */
		gsl_vector_set(&output_data.vector, i, this_int);
	}
	element->last_buf_sum = last_val;
	gint temp_dur = g_queue_get_length(element->sum_queue);
	gsl_vector_free(buf_vector64);
	/*
 	* SEND MSG
 	*/
	gst_element_post_message(GST_ELEMENT(element), gstlal_integrate_message_new64(element, max_int_val, max_int_time));

	return result;
}


GstMessage *gstlal_integrate_message_new32(GSTLALIntegrate *element, gfloat int_max, gdouble int_max_time)
{
	GstStructure *s = gst_structure_new(
		"integrate",
		"sample_rate", G_TYPE_INT, element->sample_rate,
		"template_dur", G_TYPE_DOUBLE, element->template_dur,
		"int_max", G_TYPE_FLOAT, int_max,
		"timestamp", G_TYPE_DOUBLE, int_max_time,
		NULL
	);

	GstMessage *m = gst_message_new_element(GST_OBJECT(element), s);

	return m;
}


GstMessage *gstlal_integrate_message_new64(GSTLALIntegrate *element, gdouble int_max, gdouble int_max_time)
{
	GstStructure *s = gst_structure_new(
		"integrate",
		"sample_rate", G_TYPE_INT, element->sample_rate,
		"template_dur", G_TYPE_DOUBLE, element->template_dur,
		"int_max", G_TYPE_DOUBLE, int_max,
		"timestamp", G_TYPE_DOUBLE, int_max_time,
		NULL
	);

	GstMessage *m = gst_message_new_element(GST_OBJECT(element), s);

	return m;
}

/*
 * ============================================================================
 *
 *                     GstBaseTransform Method Overrides
 *
 * ============================================================================
 */

/*
 * set_caps()
 */

static gboolean set_caps(GstBaseTransform *base, GstCaps *incaps, GstCaps *outcaps) {
	GSTLALIntegrate *element = GSTLAL_INTEGRATE (base);
	GstStructure *instruct;
	gint inrate;

	gboolean success = gst_audio_info_from_caps(&element->audio_info, outcaps);
	element->width = GST_AUDIO_INFO_WIDTH(&element->audio_info);
	element->unitsize = GST_AUDIO_INFO_BPF(&element->audio_info);
	g_object_set(element->adapter, "unit-size", element->unitsize, NULL);

	instruct = gst_caps_get_structure(incaps, 0);

	g_return_val_if_fail(gst_structure_get_int (instruct, "rate", &inrate), FALSE);

	element->sample_rate = inrate;

	element->sum_queue = g_queue_new();
	gint i;
	gint temp_length = (gint) ceil( (gdouble) element->sample_rate * element->template_dur);

	if (element->width == 32) {
		/* Fill sum_queue with zeros */
		for (i=0; i<temp_length; i++){
			gfloat *zero = (gpointer) malloc(sizeof(gfloat));
			*zero = 0.0;
			g_queue_push_head(element->sum_queue, zero);
		}
	}
	else if (element->width == 64) {
		/* Fill sum_queue with zeros */
		for (i=0; i<temp_length; i++){
			gdouble *zero = (gpointer) malloc(sizeof(gdouble));
			*zero = 0.0;
			g_queue_push_head(element->sum_queue, zero);
		}
	}
	return TRUE;
}



/*
 * start()
 */

static gboolean start(GstBaseTransform *trans)
{
	GSTLALIntegrate *element = GSTLAL_INTEGRATE(trans);

	element->last_buf_sum = 0.0;
	element->t0 = GST_CLOCK_TIME_NONE;
	return TRUE;
}


/*
 * stop()
 */

static gboolean stop(GstBaseTransform *trans)
{
	GSTLALIntegrate *element = GSTLAL_INTEGRATE(trans);

	return TRUE;
}


/*
 * transform()
 */

static GstFlowReturn transform(GstBaseTransform *trans, GstBuffer *inbuf, GstBuffer *outbuf)
{
	GSTLALIntegrate *element = GSTLAL_INTEGRATE(trans);
	GstMapInfo inmap, outmap;
	GstFlowReturn result;

	/*
 	* process buffer
 	*/
	if(GST_BUFFER_IS_DISCONT(inbuf)) {
		/*
		 * flush adapter
		 */
		GST_INFO_OBJECT(element, "flushing adapter contents");
		gst_audioadapter_clear(element->adapter);
		GST_INFO_OBJECT(element, "flushed adapter contents");
	}

	/*
	 * gap logic
	 */
	if(!GST_BUFFER_FLAG_IS_SET(inbuf, GST_BUFFER_FLAG_GAP)) {
		/*
		 * input is not 0s.
		 */
		if (element->width == 32) {
			gst_buffer_ref(inbuf);	/* don't let the adapter free it */
			gst_audioadapter_push(element->adapter, inbuf);
			guint size;
			g_object_get(element->adapter, "size", &size, NULL);

		        element->timestamp = (gdouble) GST_TIME_AS_SECONDS(GST_BUFFER_PTS(inbuf));

			gst_buffer_map(inbuf, &inmap, GST_MAP_READ);
			gst_buffer_map(outbuf, &outmap, GST_MAP_WRITE);

			/* get reference to outmap.data. *output will reference first index in outmap.data, while output will reference whole array */
			float *output = (float *) outmap.data;

			result = integrate32(element, output, &size);

			gst_buffer_unmap(outbuf, &outmap);
			gst_buffer_unmap(inbuf, &inmap);
		}
		else if (element->width == 64) {
			gst_buffer_ref(inbuf);	/* don't let the adapter free it */
			gst_audioadapter_push(element->adapter, inbuf);
			guint size;
			g_object_get(element->adapter, "size", &size, NULL);

		        element->timestamp = (gdouble) GST_TIME_AS_SECONDS(GST_BUFFER_PTS(inbuf));

			gst_buffer_map(inbuf, &inmap, GST_MAP_READ);
			gst_buffer_map(outbuf, &outmap, GST_MAP_WRITE);

			/* get reference to outmap.data. *output will reference first index in outmap.data, while output will reference whole array */
			double *output = (double *) outmap.data;

			result = integrate64(element, output, &size);

			gst_buffer_unmap(outbuf, &outmap);
			gst_buffer_unmap(inbuf, &inmap);
		}
	} 
	else {
		/*
		 * input is 0s.
		 */
		GST_INFO_OBJECT(element, "following gap logic");
		gint i;
		gint64 in_length = GST_BUFFER_OFFSET_END(inbuf) - GST_BUFFER_OFFSET(inbuf);
		
		/* Flush integral history by filling sum_queue with zeros the same length as the gap */
		if (element->width == 64) {
			for (i=0; i<in_length; i++){
				gdouble *zero = g_queue_pop_tail(element->sum_queue);
				*zero = element->last_buf_sum;
				g_queue_push_head(element->sum_queue, zero);
			}
		}
		else if (element->width == 32) {
			for (i=0; i<in_length; i++){
				gfloat *zero = g_queue_pop_tail(element->sum_queue);
				*zero = element->last_buf_sum;
				g_queue_push_head(element->sum_queue, &zero);
			}
		}
		/*Do NOT reset last_buf_sum bc we still want a continuous cumulative sum*/
		result = GST_FLOW_OK;
	}


	/* flush the adapter by the number of samples available in the adapter */
	guint size;
	g_object_get(element->adapter, "size", &size, NULL);
	gst_audioadapter_flush_samples(element->adapter, size);

	
	/*
 	* done
 	*/
	return result;
}
/*
 * ============================================================================
 *
 *                          GObject Method Overrides
 *
 * ============================================================================
 */

/*
 * Utility function for freeing our filename queue elements.
 */
static void free_queue_element (void *data, void *userData) 
{
  g_free(data);
  }


/*
 * properties
 */

enum property {
	ARG_TEMPLATE_DUR = 1
};


static void set_property(GObject *object, enum property id, const GValue *value, GParamSpec *pspec)
{

	GSTLALIntegrate *element = GSTLAL_INTEGRATE(object);

	GST_OBJECT_LOCK(element);

	switch (id) {
	case ARG_TEMPLATE_DUR:{
		element->template_dur = g_value_get_double(value);
		break;
		
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, id, pspec);
		break;
		}
	}
	GST_OBJECT_UNLOCK(element);
}


static void get_property(GObject *object, enum property id, GValue *value, GParamSpec *pspec)
{
	GSTLALIntegrate *element = GSTLAL_INTEGRATE(object);

	GST_OBJECT_LOCK(element);
	switch (id) {
	case ARG_TEMPLATE_DUR:
		g_value_set_double(value, element->template_dur);
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, id, pspec);
		break;
	}

	GST_OBJECT_UNLOCK(element);
}


/*
 * finalize()
 */

static void finalize(GObject *object)
{
	GSTLALIntegrate *element = GSTLAL_INTEGRATE(object);
	g_queue_foreach(element->sum_queue, &free_queue_element, NULL);
	g_queue_free(element->sum_queue);
	G_OBJECT_CLASS(gstlal_integrate_parent_class)->finalize(object);
}


/*
 * class_init()
 */

static void gstlal_integrate_class_init(GSTLALIntegrateClass *klass)
{

	GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
	GstElementClass *element_class = GST_ELEMENT_CLASS(klass);
	GstBaseTransformClass *transform_class = GST_BASE_TRANSFORM_CLASS(klass);

	gst_element_class_set_details_simple(element_class, "Integrator", "Filter/Audio", "Integrates for every point in timeseries back a dur amount of time.", "Rachael Huxford <rachael.huxford@ligo.org>, Chad Hanna <chad.hanna@ligo.org>, Patrick Godwin <patrick.godwin@ligo.org>");
	gobject_class->set_property = GST_DEBUG_FUNCPTR(set_property);
	gobject_class->get_property = GST_DEBUG_FUNCPTR(get_property);
	gobject_class->finalize = GST_DEBUG_FUNCPTR(finalize);

	transform_class->set_caps = GST_DEBUG_FUNCPTR(set_caps);
	transform_class->transform = GST_DEBUG_FUNCPTR(transform);
	transform_class->start = GST_DEBUG_FUNCPTR(start);
	transform_class->stop = GST_DEBUG_FUNCPTR(stop);

	g_object_class_install_property(
		gobject_class,
		ARG_TEMPLATE_DUR,
		g_param_spec_double(
		"template-dur",
		"Template Duration",
		"The average length of the templates in the current bin.",
		0.0, G_MAXDOUBLE, 0.0,
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT
		)
	);

	gst_element_class_add_pad_template(element_class, gst_static_pad_template_get(&src_template));
	gst_element_class_add_pad_template(element_class, gst_static_pad_template_get(&sink_template));
}


/*
 * init()
 */

static void gstlal_integrate_init(GSTLALIntegrate *element)
{
	gst_base_transform_set_gap_aware(GST_BASE_TRANSFORM(element), TRUE);
	element->adapter = g_object_new(GST_TYPE_AUDIOADAPTER, NULL); 
}
