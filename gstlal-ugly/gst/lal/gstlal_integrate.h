/*
 * Timeseries Integrator 
 *
 * Copyright (C) 2022 Rachael Huxford, Chad Hanna
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef __GSTLAL_INTEGRATE_H__
#define __GSTLAL_INTEGRATE_H__

#include <glib.h>
#include <gst/gst.h>
#include <gst/base/gstbasetransform.h>
#include <gstlal/gstaudioadapter.h>


#include <lal/FrequencySeries.h>
#include <lal/LALDatatypes.h>
#include <lal/TimeFreqFFT.h>
#include <lal/Units.h>


G_BEGIN_DECLS


/*
 * lal_whiten element
 */


#define GSTLAL_INTEGRATE_TYPE \
	(gstlal_integrate_get_type())
#define GSTLAL_INTEGRATE(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST((obj), GSTLAL_INTEGRATE_TYPE, GSTLALIntegrate))
#define GSTLAL_INTEGRATE_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_CAST((klass), GSTLAL_INTEGRATE_TYPE, GSTLALIntegrateClass))
#define GST_IS_GSTLAL_INTEGRATE(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE((obj), GSTLAL_INTEGRATE_TYPE))
#define GST_IS_GSTLAL_INTEGRATE_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_TYPE((klass), GSTLAL_INTEGRATE_TYPE))


typedef struct _GSTLALIntegrate GSTLALIntegrate;
typedef struct _GSTLALIntegrateClass GSTLALIntegrateClass;


/**
 * GSTLALIntegrate:
 */


struct _GSTLALIntegrate {
	GstBaseTransform element;

	/*
	 * input stream
	 */
	GstAudioInfo audio_info;
	GstAudioAdapter *adapter;
	gint width;
	gsize unitsize;
	gdouble timestamp;

	gdouble template_dur;
	guint sample_rate;


	/*
	 * work space
	 */
	
	gsl_vector_float *buf_vector32;
	gsl_vector *buf_vector64;
	GQueue *sum_queue;
	double last_buf_sum;
	GstClockTime t0;

};


/**
 * GSTLALIntegrateClass:
 * @parent_class:  the parent class
 */


struct _GSTLALIntegrateClass {
	GstBaseTransformClass parent_class;
};


GType gstlal_integrate_get_type(void);

/*
 * ============================================================================
 *
 *                                Exported API
 *
 * ============================================================================
 */


static GstMessage *gstlal_integrate_message_new64(GSTLALIntegrate *element, gdouble int_max, gdouble int_max_time);
static GstMessage *gstlal_integrate_message_new32(GSTLALIntegrate *element, gfloat int_max, gdouble int_max_time);

REAL8FrequencySeries *gstlal_integrate_message_parse(GstMessage *m);


G_END_DECLS


#endif	/* __GSTLAL_INTEGRATE_H__ */
