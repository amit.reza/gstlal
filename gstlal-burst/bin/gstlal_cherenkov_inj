#!/usr/bin/env python3
#
# Copyright (C) 2021  Soichiro Kuwahara
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

"""Cherenkov burst injection tool"""

#
# =============================================================================
#
#                                   Preamble
#
# =============================================================================
#


import math
from optparse import OptionParser
import random
import sys

import lal

from ligo.lw import ligolw
from ligo.lw import lsctables
from ligo.lw import utils as ligolw_utils
from ligo.lw.utils import process as ligolw_process
from ligo.lw.utils import segments as ligolw_segments
from ligo.segments import utils as segments_utils


@lsctables.use_in
class LIGOLWContentHandler(ligolw.LIGOLWContentHandler):
	pass


__author__ = "Soichrio Kuwahara <soichiro.kuwahara@ligo.org>"

program_name = "gstlal_cherenkov_inj"


#
# =============================================================================
#
#                                 Command Line
#
# =============================================================================
#


def parse_command_line():
	parser = OptionParser(
		description = "GstLAL-based cherenkov burst injection pipeline."
	)
	parser.add_option("--dEoverdA", type = "float", help = "Set dEoverdA. Non input of this generates uniform random amplitude.")
	parser.add_option("--deltaT", metavar = "s", default = 0, type = "float", help = "Set the time interval of injections. Default value is 0 for injecting only one.")
	parser.add_option("--start", type = "float", help = "start gps time of the injection. Non input will generate the injection in whole segements.")
	parser.add_option("--stop", type = "float", help = "end gps time of the injection. Non input will generate the injection in whole segements.")
	parser.add_option("--min-instruments", metavar = "num", default = 1, type = "int", help = "When constructing the segment list, require at least this many instruments to be on (default = 1).  See also --segments-file and --segments-name.")
	parser.add_option("--segments-file", metavar = "filename", help = "Load the segment list for the injections from this file (required).  See also --segments-name and --min-instruments.")
	parser.add_option("--segments-name", metavar = "name", default = "datasegments", help = "Use the segments with this name in the segments file (default = \"datasegments\")).  See also --segments-file and --min-instruments")
	parser.add_option("--time-slide-file", metavar = "filename", help = "Associate injections with the first time slide ID in this XML file (required).")
	parser.add_option("--output", metavar = "filename", help = "Set the name of the output file (default = stdout).")
	parser.add_option("--verbose", action = "store_true", help = "Be verbose.")
	options, filenames = parser.parse_args()

	# save for the process_params table
	process_params = dict(options.__dict__)

	# check for params
	required_options = set(("min_instruments", "segments_file", "segments_name", "output", "time_slide_file"))
	missing_options = set(option for option in required_options if getattr(options, option) is None)
	if missing_options:
		raise ValueError("missing required option(s) %s" % ", ".join("--%s" % option.replace("_", "-") for option in missing_options))

	return options, process_params, filenames


#
# =============================================================================
#
#                                     Main
#
# =============================================================================
#


options, process_params, filenames = parse_command_line()


#
# use the time-slide file to start the output document
#


xmldoc = ligolw_utils.load_filename(options.time_slide_file, verbose = options.verbose, contenthandler = LIGOLWContentHandler)


#
# add our metadata
#


process = ligolw_process.register_to_xmldoc(xmldoc, "cherenkov_burst_inj", process_params)


#
# use whatever time slide vector comes first in the table (lazy)
#


time_slide_table = lsctables.TimeSlideTable.get_table(xmldoc)
time_slide_id = time_slide_table[0].time_slide_id
offset_vector = time_slide_table.as_dict()[time_slide_id]
if options.verbose:
	print("associating injections with time slide (%d) %s" % (time_slide_id, offset_vector), file = sys.stderr)

#
# construct the segment list for injection times
#


segmentlists = ligolw_segments.segmenttable_get_by_name(ligolw_utils.load_filename(options.segments_file, contenthandler = LIGOLWContentHandler, verbose = options.verbose), options.segments_name)
segmentlists.coalesce()

if set(segmentlists) != set(offset_vector):
	raise ValueError("time slide had instruments %s but segment list has instruments %$.  must be the same." % (", ".join(sorted(offset_vector)), ", ".join(sorted(segmentlists))))

# injections are done so that they are coincident when the data has the
# given offset vector applied.  so we apply the offset vector to the
# segment lists, determine when the number of on instruments meets the
# --min-instruments criterion, and then *without restoring the offsets* use
# that segment list for the injections.  when, later, the injection code
# performs these injections the injection times will have the opposite time
# shift applied which will place them back into the available segments in
# the respective detectors.

segmentlists.offsets.update(offset_vector)
segmentlist = segments_utils.vote(segmentlists.values(), options.min_instruments)
if not abs(segmentlist):
	raise ValueError("the --min-instruments criterion cannot be satisfied")


#
# find or add a sim_burst table
#


try:
	lsctables.SimBurstTable.get_table(xmldoc)
except ValueError:
	# no sim_burst table in document
	pass
else:
	raise ValueError("%s contains a sim_burst table.  this program isn't smart enough to deal with that." % options.time_slide_xml)

sim_burst_tbl = xmldoc.childNodes[-1].appendChild(lsctables.New(lsctables.SimBurstTable, ["process:process_id", "simulation_id", "time_slide:time_slide_id", "waveform", "waveform_number", "ra", "dec", "psi", "q", "hrss", "time_geocent_gps", "time_geocent_gps_ns", "time_geocent_gmst", "duration", "frequency", "bandwidth", "egw_over_rsquared", "amplitude", "pol_ellipse_angle", "pol_ellipse_e"]))


#
# populate the sim_burst table with injections
#

if options.start is None and options.stop is None:
	start, stop = segmentlist.extent()
else:
	start = options.start
	stop = options.stop
for i in range(math.ceil(float(start) / options.deltaT), math.ceil(float(stop) / options.deltaT)):
	# we use floating point arithmetic to compute the times because we
	# don't really care when they occur, not to the nanosecond.
	time_geocent = lal.LIGOTimeGPS(i * options.deltaT)
	# skip injections outside the desired segments
	if time_geocent not in segmentlist:
		continue
	# add an injection at the desired time
	sim_burst_tbl.append(sim_burst_tbl.RowType(
		# metadata
		process_id = process.process_id,
		simulation_id = sim_burst_tbl.get_next_id(),
		time_slide_id = time_slide_id,
		waveform = "Cherenkov",

		# waveform parameters
		time_geocent = time_geocent,
		frequency = math.nan,
		# bandwidth parameter is actually not used. Hence, the length of Enterprise in StarTrek.is inputted as a fixed parameter.
		bandwidth = 641.,
		egw_over_rsquared = math.nan,

		# sky location and polarization axis orientation
		ra = random.uniform(0., 2. * math.pi),
		dec = math.asin(random.uniform(-1., 1.)),
		psi = random.uniform(0., 2. * math.pi),

		# unnecessary columns
		waveform_number = 0.,
		# Create the uniform and random amplitude in log10.
		amplitude = pow(10, random.uniform(-1., 5.)) if options.dEoverdA is None else options.dEoverdA,
		duration = math.nan,
		q = math.nan,
		hrss = math.nan,
		pol_ellipse_angle = math.nan,
		pol_ellipse_e = math.nan
	))


#
# write output
#


ligolw_utils.write_filename(xmldoc, options.output, verbose = options.verbose)
