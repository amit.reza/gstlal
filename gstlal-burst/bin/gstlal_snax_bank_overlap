#!/usr/bin/env python3

# Copyright (C) 2018 Patrick Godwin
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

"""
A program that measures template overlaps and how templates are spread out in the parameter space
for gstlal_snax_extract
"""

####################
# 
#     preamble
#
####################   


import itertools
from optparse import OptionParser
import os
import random
import sys
from urllib.parse import urljoin

import numpy

import lal
from ligo.scald import report

from gstlal import aggregator
from gstlal.plots import util as plotutil
from gstlal.snax import utils

import matplotlib
matplotlib.use('Agg')
from mpl_toolkits.axes_grid import make_axes_locatable
from matplotlib.colorbar import Colorbar
from matplotlib import pyplot as plt
from matplotlib import ticker as tkr
import matplotlib.cm as cm

matplotlib.rcParams.update({
	"font.size": 13.0,
	"axes.titlesize": 13.0,
	"axes.labelsize": 13.0,
	"xtick.labelsize": 13.0,
	"ytick.labelsize": 13.0,
	"legend.fontsize": 13.0,
	"figure.dpi": 300,
	"savefig.dpi": 300,
	"text.usetex": False,
	"path.simplify": True
})

colors = ['#2c7fb8', '#e66101', '#5e3c99', '#d01c8b']

####################
#
#    functions
#
####################

def plot_waveform(time, waveform, waveform_type='', waveform_params=None):

	fig, axes = plt.subplots()

	axes.plot(time, waveform, color = '#2c7fb8', alpha = 0.7, lw=2)

	axes.set_title(r"%s waveform" % (waveform_type))
	axes.set_ylabel(r"Amplitude [arb. unit]")
	axes.set_xlabel(r"Time [seconds]")
	axes.set_xlim(time[0], time[-1])
	if waveform_params:
		axes.text(0.96 * max(time), 0.98 * min(waveform), r"%s" % repr(waveform_params), size=10, ha='right')

	return fig

def plot_waveforms(times, waveforms, waveform_type='', waveform_params=None):
	"""
	Plots multiple waveforms in one plot (up to 4 at one time)
	"""
	assert len(times) <= 4

	# determine waveform limits
	amp_min = min(numpy.min(waveform) for waveform in waveforms)
	amp_max = max(numpy.max(waveform) for waveform in waveforms)

	fig, axes = plt.subplots(len(times), sharex=True, sharey=True)

	for ax, key, color in zip(axes, truedat.keys(), colors):

		ax.plot(time, timeseries, color = color, alpha = 0.7, lw=2)
		ax.set_ylim(amp_min, amp_max)
		ax.set_xlim(time[0], time[-1])
		ax.set_xlabel(r"Time [seconds]")
		if waveform_params:
			ax.text(0.98 * max(times), 0.97 * amp_min, r"%s" % repr(waveform_params), size=10, ha='right')

	axes[0].set_title(r"Waveforms")

	fig.text(0.04, 0.5, r"Amplitude [arb. unit]", ha='center', va='center', rotation='vertical')

	fig.subplots_adjust(hspace=0)
	plt.setp([a.get_xticklabels() for a in fig.axes[:-1]], visible=False)

	return fig

def plot_template_bank(waveform_param1, waveform_param2, overlaps, waveform_type='', waveform_params=None):

	fig, axes = plt.subplots()

	axes.scatter(waveform_param1, waveform_param2, c = overlaps, cmap = cm.coolwarm, alpha = 0.8, lw=0)
	norm = matplotlib.colors.Normalize(vmin=min(overlaps), vmax=numpy.max(overlaps), clip=True)

	axes.set_title(r"Template Bank Placement for %s" % (waveform_type))
	axes.set_xlabel(r"%s" % waveform_params[0])
	axes.set_ylabel(r"%s" % waveform_params[1])
	axes.set_xlim(min(waveform_param1) - 0.1 * min(waveform_param1), 1.1 * max(waveform_param1))
	axes.set_ylim(min(waveform_param2) - 0.1 * min(waveform_param2), 1.1 * max(waveform_param2))
	axes.loglog()

	# set up colorbar
	divider = make_axes_locatable(axes)
	cax = divider.append_axes( "right", size="5%", pad=0.1)
	cbl = matplotlib.colorbar.ColorbarBase(cax, cmap = cm.coolwarm, norm=norm, orientation="vertical")
	cbl.set_label(r"Overlap")

	plt.tight_layout()


	return fig

def generate_html_file(plot_paths, output_dir, waveform_type=''):
	if options.verbose:
		print("Creating html report...", file=sys.stderr)

	channels = set()
	page = report.Report('SNAX Bank')

	# header
	summary = report.Tab('Summary')
	summary += report.Header(f'Waveform Report for {waveform_type}')

	# plots
	for plot in sorted(plot_paths):
		summary += report.Image(os.path.join(output_dir, plot))

	page += summary

	# generate page
	page.save(output_dir)
	if options.verbose:
		print("done.", file=sys.stderr)


###############################
# 
#       command line parser
#
###############################

def parse_command_line():

	parser = OptionParser(usage = '%prog [options]', description = __doc__)

	parser.add_option("-v", "--verbose", action = "store_true", help = "Be verbose.")
	parser.add_option("-m", "--mismatch", type = "float", default = 0.05, help = "Mismatch between templates, mismatch = 1 - minimal match. Default = 0.05.")
	parser.add_option("-q", "--qhigh", type = "float", default = 100, help = "Q high value for half sine-gaussian waveforms. Default = 100.")
	parser.add_option("--output-dir", metavar = "filename", default = ".", help = "Set the location of the output (plots, etc).")
	parser.add_option("--cluster", help = "Set the cluster that this script is being run on (for proper public_html linking)")
	parser.add_option("--waveform", default = "sine_gaussian",  help = "Set the type of waveform to plot. options=[sine_gaussian, half_sine_gaussian].")

	# parse the arguments and sanity check
	options, args = parser.parse_args()

	return options, args

####################
# 
#       main
#
####################   

if __name__ == '__main__':
	options, args = parse_command_line()

	# create directory if it doesn't exist
	aggregator.makedir(options.output_dir)

    # common parameters we will use throughout
	max_samp_rate = 8192
	min_samp_rate = 32
	n_rates = int(numpy.log2(max_samp_rate/min_samp_rate) + 1)

	if options.verbose:
		print("Creating templates...", file=sys.stderr)

	# generate templates for each rate considered
	rates = [min_samp_rate*2**i for i in range(n_rates)]
	downsample_factor = 0.8
	qhigh = options.qhigh
	qlow = 3.3166
	fhigh = max_samp_rate / 2.
	flow = min_samp_rate / 4.

	if options.waveform == 'sine_gaussian':
		waveforms = utils.SineGaussianGenerator((flow, fhigh), (qlow, qhigh), rates, mismatch = options.mismatch, downsample_factor=downsample_factor)
	elif options.waveform == 'half_sine_gaussian':
		waveforms = utils.HalfSineGaussianGenerator((flow, fhigh), (qlow, qhigh), rates, mismatch = options.mismatch, downsample_factor=downsample_factor)
	else:
		raise NotImplementedError
	basis_params = waveforms.parameter_grid
	templates = {rate: [waveform for waveform in waveforms.generate_templates(rate, quadrature=False)] for rate in rates}

	# get all templates and params
	all_templates = list(itertools.chain(*templates.values()))
	all_params = list(itertools.chain(*basis_params.values()))

	if options.verbose:
		print("Creating template overlaps...", file=sys.stderr)

	# zero pad templates to make them the same length
	max_sample_pts = max(len(template) for template in all_templates)
	all_templates = [numpy.pad(template, ((max_sample_pts - len(template)) // 2, (max_sample_pts - len(template)) // 2), 'constant') for template in all_templates]

	# calculate overlap for each template and find maximum
	overlaps = []
	for this_template in all_templates:
		overlaps.append(max([numpy.dot(this_template,template) for template in all_templates if not numpy.array_equal(template, this_template)]))

	print("total number of templates: %d" % len(all_templates), file=sys.stderr)
	print("min overlap specified: %f" % (1 - options.mismatch), file=sys.stderr)
	print("max template overlap: %f" % max(overlaps), file=sys.stderr)
	print("min template overlap: %f" % min(overlaps), file=sys.stderr)

	# generate template plots
	plot_paths = []

	# cast params to a nicer format
	# FIXME: should really be passing a dict of params instead
	param_names = ['f', 'Q', 'duration']
	waveform_type = options.waveform.replace('_', ' ').title()

	# limit the number of waveforms plotted per frequency band
	num_samples = 3

	if options.verbose:
		print("Creating waveform plots...", file=sys.stderr)

	for rate in rates:
		#for template_id, template in enumerate(random.sample(templates[rate], num_samples)):
		for template_id in random.sample(numpy.arange(len(templates[rate])), num_samples):
			waveform_params = ["%s: %.3f" % (name, param) for param, name in zip(basis_params[rate][template_id], param_names)]
			template = templates[rate][template_id]

			if options.verbose:
				print("\tCreating waveform plot with parameters: %s" % repr(waveform_params), file=sys.stderr)

			series_fig = plot_waveform(waveforms.times[rate], template, waveform_type, waveform_params)
			fname = 'plot_%s_%s-timeseries.png' % (str(rate).zfill(4), str(template_id).zfill(4))
			plot_paths.append(fname)
			series_fig.savefig(os.path.join(options.output_dir, fname))
			plt.close(fname)

	# generate template overlap map
	freqs = [param[0] for param in all_params]
	Qs = [param[1] for param in all_params]

	if options.verbose:
		print("Creating template overlap plot...", file=sys.stderr)

	overlap_fig = plot_template_bank(freqs, Qs, overlaps, waveform_type, param_names[:2])
	fname = 'plot-template_overlap.png'
	plot_paths.append(fname)
	overlap_fig.savefig(os.path.join(options.output_dir, fname))
	plt.close(fname)

	# generate html page
	generate_html_file(plot_paths, options.output_dir, waveform_type=waveform_type)
